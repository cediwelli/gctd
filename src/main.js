const imageSmiley	= document.getElementById('smiley-image');
const imageDNF		= document.getElementById('dnf-image');
const imageTradi	= document.getElementById('tradi-image');
const imageMulti	= document.getElementById('multi-image');
const imageMystery	= document.getElementById('mystery-image');
const imageEarth	= document.getElementById('earth-image');
const imageBG		= document.getElementById('bg-image');
const imageBGFriedhof	= document.getElementById('bg-friedhof-image');
const imageMenu		= document.getElementById('menu-image');
const imageCoords	= document.getElementById('coords-image');
const imageVerloren	= document.getElementById('verloren-image');
const imageGewonnen	= document.getElementById('gewonnen-image');

const buttonStartWave	= document.getElementById('btn-start-wave');
const buttonBuyTradi	= document.getElementById('btn-buy-tradi');
const buttonBuyMulti	= document.getElementById('btn-buy-multi');
const buttonBuyMystery	= document.getElementById('btn-buy-mystery');
const buttonBuyEarth	= document.getElementById('btn-buy-earth');

const buttonBack	= document.getElementById('btn-back');
const buttonManual	= document.getElementById('btn-manual');
const buttonCoords	= document.getElementById('btn-coordinates');
const buttonSelectMaps	= document.getElementById('btn-select-map');
const buttonMapRiver	= document.getElementById('btn-select-map-grass');
const buttonMapHalloween= document.getElementById('btn-select-map-halloween');

const buttonsMapMenu	= document.getElementsByClassName('map-menu');
const buttonsMainMenu	= document.getElementsByClassName('main-menu');

const containerCoords	= document.getElementById('coords-container');
const listCoords	= document.getElementById('list-coords');

const chkShowAOE = document.getElementById('chk-show-aoe');

const canvas	= document.getElementById('canvas');
const ctx	= canvas.getContext('2d');

const itemNameSavedCoords = 'saved-coords';

const X = 1;

const pixel_size = 40;

const textStatus = document.getElementById('text-status');
var hud_ticks	= 0;
var hud_text	= "";

var running	= true;
var draw_grid	= true;
var draw_grid_forbidden = false;
var draw_path = false;

var time_start	= Date.now();
var time_end	= 0;
var time_delta	= 0;

const PHASE_PLAY	= 0;
const PHASE_PLACE	= 1;
const PHASE_MENU	= 2;
const PHASE_LOSE	= 3;
const PHASE_WIN		= 4;

const MENU_MAIN		= 0;
const MENU_SEL_MAPS	= 1;
const MENU_MANUAL	= 2;
const MENU_GAME		= 3;
const MENU_COORDS	= 4;

var menu_state = MENU_MAIN;

var game	= null;
var map_river	= null;
var map_halloween = null;

var enemy_types = [
	// SPEED, HEALTH, IMG
	[2, 1, imageDNF],
	[2, 2, imageDNF],
	[3, 4, imageDNF],
	[5, 3, imageDNF],
	[15, 1, imageDNF],
	[9, 3, imageDNF],
	[10, 4, imageDNF]
	/*[3, 2, imageDNF],
	[4, 1, imageDNF],
	[2, 4, imageDNF],
	[1, 6, imageDNF],
	[10, 5, imageDNF]*/
];

var river_wave_0 = [
	// ENEMY TYPE, SPAWN AT TICKS
];

var river_wave_1 = [
	// ENEMY TYPE, SPAWN AT TICKS
	[0, 10],
	[0, 50],
	[0, 90],
	[0, 130],
	[0, 170],
	[0, 210],
	[0, 280]
];

var river_wave_2 = [
	// ENEMY TYPE, SPAWN AT TICKS
	[1, 10],
	[1, 30],
	[1, 60],
	[1, 90],
	[1, 100],
	[1, 140],
	[1, 160],
	[1, 180],
	[1, 190],
	[1, 210],
	[1, 250],
	[1, 260]

];

var river_wave_3 = [
	// ENEMY TYPE, SPAWN AT TICKS
	[2, 10],
	[2, 20],
	[2, 30],
	[2, 40],
	[2, 50],
	[2, 60],
	[2, 70],
	[2, 80],
	[2, 90],
	[2, 100],
];

var river_wave_4 = [
	// ENEMY TYPE, SPAWN AT TICKS
	[2, 10],
	[1, 20],
	[2, 50],
	[2, 80],
	[1, 95],
	[2, 105],
	[2, 120],
	[1, 140],
	[2, 160],
	[2, 170],
	[2, 190],
	[2, 210],
	[2, 230],
	[1, 260],
	[2, 290],
	[2, 300],
	[2, 320],
	[2, 350],
	[2, 355],
	[2, 360]
];

var river_wave_5 = [
	// ENEMY TYPE, SPAWN AT TICKS
	[3, 10],
	[3, 20],
	[3, 50],
	[3, 80],
	[3, 95],
	[3, 105],
	[3, 120],
	[3, 140],
	[3, 160],
	[3, 170],
	[3, 190],
	[3, 210],
	[3, 230],
	[3, 260],
	[3, 290],
	[3, 300],
	[3, 320],
	[3, 350],
	[3, 355],
	[3, 360],
	[3, 370],
	[3, 390],
	[3, 410],
	[3, 420],
	[3, 450],
	[3, 480],
	[3, 500],
	[3, 510],
	[3, 540],
	[3, 550],
	[3, 570],
	[3, 600],
	[3, 610],
	[3, 630],
	[3, 650],
	[3, 680],
	[3, 700]
];

var river_wave_6 = [
	// ENEMY TYPE, SPAWN AT TICKS
	[4, 10],
	[4, 15],
	[4, 20],
	[4, 25],
	[4, 30],
	[4, 35],
	[4, 40],
	[4, 45],
	[4, 50],
	[4, 55],
	[4, 60],
	[4, 65],
	[4, 70],
	[4, 75],
	[4, 80],
	[4, 85],
	[4, 90],
	[4, 95],
	[4, 100],
	[4, 200],
	[4, 205],
	[4, 210],
	[4, 220],
	[4, 230],
	[4, 240],
	[4, 250],
	[4, 260],
	[4, 270],
	[4, 280],
	[4, 290],
	[4, 300],
];

var river_wave_7 = [
	// ENEMY TYPE, SPAWN AT TICKS
	[5, 10],
	[5, 20],
	[5, 40],
	[5, 50],
	[5, 80],
	[5, 100],
	[5, 105],
	[5, 110],
	[5, 125],
	[5, 140],
	[5, 160],
	[5, 180],
	[5, 190],
	[5, 195],
	[5, 200],
	[5, 220],
	[5, 240],
	[5, 260],
	[5, 265],
	[5, 267],
	[5, 269],
	[5, 280],
	[5, 290],
	[5, 300],
	[5, 310],
	[5, 315],
	[5, 325],
	[5, 350],
	[5, 360],
	[5, 380],
	[5, 390],
	[5, 395],
	[5, 410],
	[5, 420],
	[5, 425],
	[5, 440],
	[5, 460],
	[5, 470],
	[5, 480],
	[5, 490],
];

var river_wave_8 = [
	// ENEMY TYPE, SPAWN AT TICKS
	[6, 10],
	[6, 20],
	[6, 40],
	[6, 50],
	[6, 80],
	[6, 100],
	[6, 105],
	[6, 110],
	[6, 125],
	[6, 140],
	[6, 160],
	[6, 180],
	[6, 190],
	[6, 195],
	[6, 200],
	[6, 220],
	[6, 240],
	[6, 260],
	[6, 265],
	[6, 267],
	[6, 269],
	[6, 280],
	[6, 290],
	[6, 300],
	[6, 310],
	[6, 315],
	[6, 325],
	[6, 350],
	[6, 360],
	[6, 380],
	[6, 390],
	[6, 395],
	[6, 410],
	[6, 420],
	[6, 425],
	[6, 440],
	[6, 460],
	[6, 470],
	[6, 480],
	[6, 490],
	[6, 550],
	[6, 580],
	[6, 600],
	[6, 630],
	[6, 660],
	[6, 690],
	[6, 700],
	[6, 730],
	[6, 760],
	[6, 790],
	[6, 800],
	[6, 830],
	[6, 860],
	[6, 890],
	[6, 900],
	[6, 930],
	[6, 960],
	[6, 990],
	[6, 1000],
	[6, 1030],
	[6, 1060],
];

var river_wave_9 = [
	// ENEMY TYPE, SPAWN AT TICKS

];

var river_wave_10 = [
	// ENEMY TYPE, SPAWN AT TICKS

];

var river_waves = [
	river_wave_0,
	river_wave_1,
	river_wave_2,
	river_wave_3,
	river_wave_4,
	river_wave_5,
	river_wave_6,
	river_wave_7,
	river_wave_8,
	river_wave_9,
	river_wave_10
	//river_wave_1,
	//river_wave_1,
];

var halloween_waves = [
	river_wave_0,
	river_wave_1,
	river_wave_1,
	river_wave_1,
	river_wave_1,
	river_wave_1,
	river_wave_1,
	river_wave_1,
	river_wave_1,
	river_wave_1,
	river_wave_1,
	river_wave_1,
	river_wave_1,
	river_wave_1,
	river_wave_1,
	river_wave_1,
	river_wave_1,
	river_wave_1,
	river_wave_1,
	river_wave_1,
	river_wave_1,
	river_wave_1,
	river_wave_1,
	river_wave_1,
	river_wave_1,
	river_wave_1,
	river_wave_1,
	river_wave_1,
	river_wave_1,
	river_wave_1,
	river_wave_1,
];

function removeFromArray(array, index) {

	let before = array.slice(0, index);
	let after = [];

	if(index + 1 < array.length) {
		after = array.slice(index+1);
	}

	let both = before.concat(after);
	return both;
}

function createCoordsListElement(coordObj) {

	/*
	 * <li>
						<strong><span class="coords-title">Koordinaten für GC12345:</span><strong><br />
						<span class="coords coords-north">17.989</span><br />
						<span class="coords coords-east">17.989</span>
					</li>
	 * */

	let li		= document.createElement('li');
	let strong	= document.createElement('strong');
	let span_title	= document.createElement('span');
	let span_north	= document.createElement('span');
	let span_east	= document.createElement('span');
	let br_1	= document.createElement('br');
	let br_2	= document.createElement('br');

	span_title.classList.add('coords-title');
	span_title.innerText = "Koordinaten für " + coordObj.gc_code + ":";

	span_north.classList.add('coords');
	span_north.classList.add('coords-north');

	span_east.classList.add('coords');
	span_east.classList.add('coords-east');

	span_north.innerText = coordObj.north;
	span_east.innerText = coordObj.east;

	strong.appendChild(span_title);

	li.appendChild(strong);
	li.appendChild(br_1);
	li.appendChild(span_north);
	li.appendChild(br_2);
	li.appendChild(span_east);

	listCoords.appendChild(li);
}

function loadCoordsFromLocalStorage() {

	let string = localStorage.getItem(itemNameSavedCoords);
	let coords = JSON.parse(string);

	listCoords.innerHTML = "";

	for(let i = 0; i < coords.length; ++i) {
		createCoordsListElement(coords[i]);
	}
}

class Coords {
	constructor(gc_code, north, east) {
		this.gc_code = gc_code;
		this.north = north;
		this.east = east;
	}
}

class Point {
	constructor(x, y) {
		this.x = x;
		this.y = y;
	}
}

class Path {

	constructor(point_start, ctrl_start, point_end, ctrl_end) {
		this.point_start = point_start;
		this.point_end = point_end;
		this.ctrl_start = ctrl_start;
		this.ctrl_end = ctrl_end;
	}

	getPosAt(progress) {

		var waypoint_start_ctrl = new Point(
			(this.ctrl_start.x - this.point_start.x)*progress + this.point_start.x,
			(this.ctrl_start.y - this.point_start.y)*progress + this.point_start.y
		);

		var waypoint_ctrl_ctrl = new Point(
			(this.ctrl_end.x - this.ctrl_start.x)*progress + this.ctrl_start.x,
			(this.ctrl_end.y - this.ctrl_start.y)*progress + this.ctrl_start.y
		);

		var waypoint_ctrl_end = new Point(
			(this.point_end.x - this.ctrl_end.x)*progress + this.ctrl_end.x,
			(this.point_end.y - this.ctrl_end.y)*progress + this.ctrl_end.y
		);



		var waypoint_startctrl_ctrlctrl = new Point(
			(waypoint_ctrl_ctrl.x - waypoint_start_ctrl.x)*progress + waypoint_start_ctrl.x,
			(waypoint_ctrl_ctrl.y - waypoint_start_ctrl.y)*progress + waypoint_start_ctrl.y
		);

		var waypoint_ctrlctrl_ctrlend = new Point(
			(waypoint_ctrl_end.x - waypoint_ctrl_ctrl.x)*progress + waypoint_ctrl_ctrl.x,
			(waypoint_ctrl_end.y - waypoint_ctrl_ctrl.y)*progress + waypoint_ctrl_ctrl.y
		);


		var waypoint_final = new Point(
			(waypoint_ctrlctrl_ctrlend.x - waypoint_startctrl_ctrlctrl.x)*progress + waypoint_startctrl_ctrlctrl.x,
			(waypoint_ctrlctrl_ctrlend.y - waypoint_startctrl_ctrlctrl.y)*progress + waypoint_startctrl_ctrlctrl.y
		);

		return waypoint_final;
	}

	getLength(iterations) {

		let last_point = this.point_start;
		let len = 0;

		for(let i = 1/iterations; i <= 1; i += 1/iterations) {
			let current_point = this.getPosAt(i);

			let x = current_point.x - last_point.x;
			let y = current_point.y - last_point.y;

			let amount = Math.sqrt(x*x + y*y);
			len += amount;

			last_point = current_point;
		}

		return len;
	}

}

class ComposedPath {

	constructor() {
		this.paths		= [];
		this.path_sections	= [];
		this.total_length	= 0;
	}

	setPaths(paths) {
		this.paths		= paths;
		this.path_sections	= [];
		this.total_length	= 0;

		for(let i = 0; i < paths.length; ++i) {
			this.total_length += paths[i].getLength(12);
		}

		for(let i = 0; i < paths.length; ++i) {

			let len = paths[i].getLength(12);
			let section_size = len;
			this.path_sections.push(section_size);
		}
	}

	getPosAt(progress) {

		progress = Math.min(1, progress);

		let length_progress = this.total_length * progress;
		let lengths = 0;

		for(let i = 0; i < this.path_sections.length; ++i) {

			lengths += this.path_sections[i];

			if(length_progress <= lengths) {
				let overshoot = lengths - length_progress;
				let section_minus_overshoot = this.path_sections[i] - overshoot;

				let section_progress = section_minus_overshoot/this.path_sections[i];

				return this.paths[i].getPosAt(section_progress);
			}
		}

		return new Point(0, 0);
	}
}

class Tower {

	constructor() {
		this.draw_aoe = false;
	}

	draw() {
		console.log("stub");
	}

	tick(game) {

	}

	spawnBullet(game, x, y, target) {

		var bullet = new Bullet(x, y, target);
		game.active_bullets.push(bullet);

	}

	setDrawAOE(b) {
		this.draw_aoe = b;
	}

}

class TradiTower extends Tower {

	constructor(x, y) {
		super();
		this.img = imageTradi;
		this.grid_x = x;
		this.grid_y = y;
		this.radius = 150;
		this.ticks_to_rest = 0;
	}

	distanceToEnemy(enemy) {

		let my_x = this.grid_x*pixel_size - pixel_size/2;
		let my_y = this.grid_y*pixel_size - pixel_size/2;

		let enemy_pos = enemy.getPosition();

		let dx = enemy_pos.x - my_x;
		let dy = enemy_pos.y - my_y;

		return Math.sqrt((dx*dx) + (dy*dy));

	}

	getNearestEnemy(game) {

		let nearest_enemies = [];
		let distances = [];

		for(let i = 0; i < game.active_enemies.length; ++i) {

			let enemy = game.active_enemies[i];
			let distance = this.distanceToEnemy(enemy);

			if(distance <= this.radius) {
				nearest_enemies.push(enemy);
				distances.push(distance);
			}
		}

		let nearest_distance = 100000000;
		let nearest_enemy_index = -1;

		for(let i = 0; i < distances.length; ++i) {

			let enemy_distance = distances[i];

			if(enemy_distance < nearest_distance) {
				nearest_distance = enemy_distance;
				nearest_enemy_index = i;
			}
		}

		if(nearest_enemy_index >= nearest_enemies.length)
			return null;

		return nearest_enemies[nearest_enemy_index];
	}

	tick(game) {

		if(this.ticks_to_rest-- <= 0 && game.tick_time%5 == 0) {

			let enemy = this.getNearestEnemy(game);

			if(enemy != null) {
				this.spawnBullet(game, this.grid_x*pixel_size + pixel_size/2, this.grid_y*pixel_size + pixel_size/2, enemy);
				this.ticks_to_rest = 60;
				console.log("shoot to kill");
			}
		}
	}

	draw() {
		ctx.drawImage(this.img, this.grid_x*pixel_size, this.grid_y*pixel_size, pixel_size, pixel_size);

		if(this.draw_aoe) {
			ctx.fillStyle = '#FF000022';
			ctx.strokeStyle = '#FF000099';
			ctx.lineWidth = 4;
			ctx.beginPath();
			ctx.arc(this.grid_x*pixel_size + pixel_size/2, this.grid_y*pixel_size + pixel_size/2, this.radius, 0, 2*Math.PI);
			ctx.fill();
			ctx.stroke();
		}
	}

}


class MultiTower extends Tower {

	constructor(x, y) {
		super();
		this.img = imageMulti;
		this.grid_x = x;
		this.grid_y = y;
		this.radius = 200;
		this.ticks_to_rest = 0;
	}

	distanceToEnemy(enemy) {

		let my_x = this.grid_x*pixel_size - pixel_size/2;
		let my_y = this.grid_y*pixel_size - pixel_size/2;

		let enemy_pos = enemy.getPosition();

		let dx = enemy_pos.x - my_x;
		let dy = enemy_pos.y - my_y;

		return Math.sqrt((dx*dx) + (dy*dy));

	}

	getNearestEnemies3(game) {

		let out = [];

		let nearest_enemies = [];
		let nearest_distances = [];

		for(let i = 0; i < game.active_enemies.length; ++i) {

			let enemy = game.active_enemies[i];
			let distance = this.distanceToEnemy(enemy);

			if(distance <= this.radius) {
				nearest_enemies.push(enemy);
				nearest_distances.push(distance);
			}
		}


		for(let i = 0; i < 3; ++i) {

			let nearest_enemy_distance = 100000000;
			let nearest_enemy_index = -1;

			for(let k = 0; k < nearest_enemies.length; ++k) {

				let d = nearest_distances[k];

				if(d < nearest_enemy_distance) {
					nearest_enemy_distance = d;
					nearest_enemy_index = k;
				}
			}

			out.push(nearest_enemies[nearest_enemy_index]);

			nearest_enemies = removeFromArray(nearest_enemies, nearest_enemy_index);
			nearest_distances = removeFromArray(nearest_distances, nearest_enemy_index);
		}

		return out;
	}

	tick(game) {

		if(this.ticks_to_rest-- <= 0 && game.tick_time%5 == 0) {

			let enemies = this.getNearestEnemies3(game);

			//console.log(enemies.length);
			for(let i = 0; i < enemies.length; ++i) {
				if(enemies[i] != null) {
					this.spawnBullet(game, this.grid_x*pixel_size + pixel_size/2, this.grid_y*pixel_size + pixel_size/2, enemies[i]);
					this.ticks_to_rest = 60;
				}
			}
		}
	}

	draw() {
		ctx.drawImage(this.img, this.grid_x*pixel_size, this.grid_y*pixel_size, pixel_size, pixel_size);

		if(this.draw_aoe) {
			ctx.fillStyle = '#FF000022';
			ctx.strokeStyle = '#FF000099';
			ctx.lineWidth = 4;
			ctx.beginPath();
			ctx.arc(this.grid_x*pixel_size + pixel_size/2, this.grid_y*pixel_size + pixel_size/2, this.radius, 0, 2*Math.PI);
			ctx.fill();
			ctx.stroke();
		}
	}
}

class MysteryTower extends Tower {

	constructor(x, y) {
		super();
		this.img	= imageMystery;
		this.grid_x	= x;
		this.grid_y	= y;
		this.radius	= 0;
		this.ticks_to_rest = 0;
	}

	tick(game) {

		if(this.ticks_to_rest-- <= 0 && game.tick_time%5 == 0) {

			if(game.active_enemies.length > 0) {

				let enemy = game.active_enemies[0];

				if(game.tick_time >= enemy.spawn_time) {
					this.spawnBullet(game, this.grid_x*pixel_size + pixel_size/2, this.grid_y*pixel_size + pixel_size/2, enemy);
					this.ticks_to_rest = 15;
				}
			}
		}
	}

	draw() {
		ctx.drawImage(this.img, this.grid_x*pixel_size, this.grid_y*pixel_size, pixel_size, pixel_size);
	}
}

class EarthTower extends Tower {

	constructor(x, y) {
		super();
		this.img	= imageEarth;
		this.grid_x	= x;
		this.grid_y 	= y;
		this.radius	= 0;
		this.ticks_to_rest = 0;
		this.mode	= 0;
	}

	tick(game) {

		if(this.ticks_to_rest-- <= 0 && game.tick_time%5 == 0) {

			for(let i = 0 ; i < game.active_enemies.length; ++i) {

				let enemy = game.active_enemies[i];

				if(enemy.spawn_time <= game.tick_time) {
					this.spawnBullet(game, this.grid_x*pixel_size + pixel_size/2, this.grid_y*pixel_size + pixel_size/2, enemy);

					if(this.mode%5 == 0) {
						this.ticks_to_rest = 120;
					} else {
						this.ticks_to_rest = 5;
					}
				}
			}

			++this.mode;
		}
	}

	draw() {
		ctx.drawImage(this.img, this.grid_x*pixel_size, this.grid_y*pixel_size, pixel_size, pixel_size);
	}

}

class Enemy {

	constructor(speed, health, img, spawn_time) {
		this.speed = speed;
		this.health = health;
		this.img = img;
		this.spawn_time = spawn_time;
		this.position_x = -1000;
		this.position_y = -1000;
		this.kill_radius = pixel_size;
		this.path_progression = 0;

		this.alive = true;
	}

	move() {
		this.position_x += this.dir_x*this.speed;
		this.position_y += this.dir_y*this.speed;
	}

	setPosition(x, y) {
		this.position_x = x;
		this.position_y = y;
	}

	getPosition() {
		return new Point(this.position_x - pixel_size/2, this.position_y - pixel_size/2);
	}

	removeHealth(amount) {
		this.health -= amount;

		if(this.health <= 0) {
			this.alive = false;
		}
	}
}

class Bullet {
	constructor(x, y, target) {
		this.position = new Point(x, y);
		this.target = target;
		this.radius = 5;
		this.speed = 15;
	}

	removeFromActive(game) {
		let index = game.active_bullets.indexOf(this);
		game.removeBullet(index);
	}

	tick(game) {

		if(this.target != null) {

			// do logic
			let vec_to_target = new Point(
				this.target.position_x - this.position.x,
				this.target.position_y - this.position.y,
			);

			let vec_len = Math.sqrt((vec_to_target.x*vec_to_target.x) + (vec_to_target.y*vec_to_target.y));
			vec_to_target.x /= vec_len;
			vec_to_target.y /= vec_len;

			this.position.x += vec_to_target.x*this.speed;
			this.position.y += vec_to_target.y*this.speed;

			if(vec_len <= this.target.kill_radius) {
				this.target.removeHealth(1);
				this.removeFromActive(game);
			}

		} else {
			this.removeFromActive(game);
		}

	}

	draw() {
		ctx.beginPath();

		ctx.fillStyle = 'darkgray';
		ctx.strokeStyle = 'black';
		ctx.arc(this.position.x - this.radius/2, this.position.y - this.radius/2, this.radius, 0, 2*Math.PI);
		ctx.fill();
		ctx.stroke();
	}
}

function drawGrid() {

}

function setHUDText(text, ticks) {
	hud_text = text;
	hud_ticks = ticks;
}

function drawTopText() {

	if(hud_ticks-- > 0) {
		ctx.fillStyle = 'white';
		ctx.strokeStyle = 'black';
		ctx.font = '50px sans';
		ctx.lineWidth = 2;

		let x = 60;
		let y = 50;

		ctx.fillText(hud_text, x, y);
		ctx.strokeText(hud_text, x, y);
	}
}

function drawPath(path) {

	ctx.fillStyle = 'green';
	ctx.fillRect(path.ctrl_start.x, path.ctrl_start.y, 10, 10);

	ctx.fillStyle = 'red';
	ctx.fillRect(path.point_start.x, path.point_start.y, 10, 10);

	ctx.fillStyle = 'blue';
	ctx.fillRect(path.ctrl_end.x, path.ctrl_end.y, 10, 10);

	ctx.fillStyle = 'orange';
	ctx.fillRect(path.point_end.x, path.point_end.y, 10, 10);

	let iterations = 12;
	let last_point = path.point_start;


	for(let i = 1/iterations; i <= 1; i += 1/iterations) {
		let current_point = path.getPosAt(i);

		ctx.strokeStyle = 'orange';
		ctx.lineWidth = 1;

		ctx.beginPath();
		ctx.moveTo(last_point.x, last_point.y);
		ctx.lineTo(current_point.x, current_point.y);
		ctx.stroke();

		//console.log("c: " + current_point.x + ", " + current_point.y);

		last_point = current_point;
	}
}

function drawComposedPath(composed_path) {
	for(let i = 0; i < composed_path.paths.length; ++i) {
		let path = composed_path.paths[i];
		drawPath(path);
	}
}

function grayButtons() {
	buttonStartWave.setAttribute('disabled', 'true');
}

function ungrayButtons() {
	buttonStartWave.removeAttribute('disabled');
}

function main() {

	createMapRiver();
	createMapHalloween();

	buttonSelectMaps.click();
	buttonBack.click();

	window.requestAnimationFrame(loop);
}

function createMapRiver() {

	var grid = [
		[X,X,X,0,0,0,0,0,0,0,0,0,X,X,X,X,X,X,X,X],
		[X,X,0,0,0,0,0,0,0,0,0,X,X,X,0,0,X,X,X,X],
		[X,X,0,0,0,0,0,0,0,0,0,X,X,0,0,0,0,0,X,X],
		[X,0,0,0,0,0,0,0,0,0,0,X,X,0,0,0,0,0,X,X],
		[0,0,0,X,X,X,X,0,0,0,0,X,X,0,0,0,0,0,0,X],
		[0,0,0,X,0,0,X,0,X,X,X,X,X,0,0,0,0,0,0,X],
		[X,X,0,X,0,0,X,0,X,0,0,0,X,X,0,0,0,0,0,X],
		[X,X,0,X,0,0,X,0,X,0,0,0,X,X,X,0,0,0,0,X],
		[X,X,X,X,X,X,X,0,X,X,0,0,X,X,X,0,0,0,0,0],
		[0,0,0,X,0,0,X,X,X,X,X,0,X,0,X,X,0,0,0,0],
		[0,0,0,X,0,X,X,0,X,X,X,0,X,0,X,X,0,0,0,0],
		[X,X,X,X,X,X,X,X,X,X,X,X,X,X,X,X,X,0,0,0],
		[X,0,0,X,0,X,X,0,0,X,X,0,X,0,X,X,X,0,0,0],
		[X,0,0,X,0,0,X,X,0,X,X,X,X,X,X,0,X,0,0,X],
		[X,X,X,X,0,0,X,X,0,0,X,X,X,X,X,X,X,0,0,X],
		[0,0,0,0,0,0,0,X,0,0,X,0,0,0,0,0,0,0,X,X],
		[X,X,0,0,0,0,X,X,0,0,X,0,0,0,0,0,0,0,0,X],
		[X,X,X,X,X,X,X,0,0,0,X,X,X,X,X,X,X,X,X,X],
		[X,X,0,0,X,X,X,X,0,0,0,0,0,0,0,0,0,0,0,0],
		[X,X,X,X,X,X,X,X,X,X,X,X,X,X,X,X,X,X,X,X],
	];

	var paths = [

		new Path(
			new Point(-10, 340),
			new Point(20, 345),
			new Point(240, 340),
			new Point(220, 345),
		),new Path(
			 new Point(240, 340),
			 new Point(260, 330),
			 new Point(263, 313),
			 new Point(263, 323),
		),new Path(
			 new Point(263, 313),
			 new Point(263, 303),
			 new Point(259, 208),
			 new Point(259, 228),
		),new Path(
			 new Point(259, 208),
			 new Point(259, 187),
			 new Point(234, 178),
			 new Point(250, 178),
		),new Path(
			 new Point(234, 178),
			 new Point(214, 178),
			 new Point(170, 178),
			 new Point(190, 178),
		),new Path(
			 new Point(170, 178),
			 new Point(150, 178),
			 new Point(139, 208),
			 new Point(139, 190),
		),new Path(
			 new Point(139, 208),
			 new Point(139, 250),
			 new Point(142, 550),
			 new Point(142, 500),
		),new Path(
			 new Point(142, 550),
			 new Point(142, 570),
			 new Point(112, 579),
			 new Point(132, 579),
		),new Path(
			 new Point(112, 579),
			 new Point(100, 579),
			 new Point(51, 582),
			 new Point(70, 582),
		),new Path(
			 new Point(51, 582),
			 new Point(30, 582),
			 new Point(19, 550),
			 new Point(19, 570),
		),new Path(
			 new Point(19, 550),
			 new Point(19, 530),
			 new Point(19, 492),
			 new Point(19, 510),
		),new Path(
			 new Point(19, 492),
			 new Point(19, 470),
			 new Point(49, 460),
			 new Point(30, 460),
		),new Path(
			 new Point(49, 460),
			 new Point(90, 460),
			 new Point(311, 458),
			 new Point(200, 458),
		),new Path(
			 new Point(311, 458),
			 new Point(331, 458),
			 new Point(340, 429),
			 new Point(340, 459),
		),new Path(
			 new Point(340, 429),
			 new Point(340, 400),
			 new Point(339, 251),
			 new Point(339, 271),
		),new Path(
			 new Point(339, 251),
			 new Point(339, 221),
			 new Point(372, 221),
			 new Point(352, 221),
		),new Path(
			 new Point(372, 221),
			 new Point(392, 221),
			 new Point(472, 222),
			 new Point(452, 222),
		),new Path(
			 new Point(472, 222),
			 new Point(492, 222),
			 new Point(504, 252),
			 new Point(504, 222),
		),new Path(
			 new Point(504, 252),
			 new Point(504, 292),
			 new Point(502, 544),
			 new Point(502, 500),
		),new Path(
			 new Point(502, 544),
			 new Point(502, 574),
			 new Point(532, 581),
			 new Point(502, 581),
		),new Path(
			 new Point(532, 581),
			 new Point(552, 581),
			 new Point(631, 581),
			 new Point(601, 581),
		),new Path(
			 new Point(631, 581),
			 new Point(651, 581),
			 new Point(661, 551),
			 new Point(661, 571),
		),new Path(
			 new Point(661, 551),
			 new Point(661, 521),
			 new Point(662, 488),
			 new Point(662, 510),
		),new Path(
			 new Point(662, 488),
			 new Point(662, 450),
			 new Point(629, 458),
			 new Point(650, 458),
		),new Path(
			 new Point(629, 458),
			 new Point(600, 458),
			 new Point(450, 458),
			 new Point(480, 458),
		),new Path(
			 new Point(450, 458),
			 new Point(430, 458),
			 new Point(421, 485),
			 new Point(421, 455),
		),new Path(
			 new Point(421, 485),
			 new Point(421, 520),
			 new Point(420, 670),
			 new Point(420, 630),
		),new Path(
			 new Point(420, 670),
			 new Point(420, 690),
			 new Point(450, 701),
			 new Point(430, 701),
		),new Path(
			 new Point(450, 701),
			 new Point(490, 701),
			 new Point(797, 701),
			 new Point(700, 701),
		)
	];

	/**********************************
	 *
	 *  Ja, hier sind die Koordinaten.
	 *  Du hast sie dir erschummelt,
	 *  bist du da drauf stolz?
	 *
	 *  Naja, nimm sie, wenn du willst.
	 *
	 * ********************************/

	var coord_prize = new Coords("GCTEST0", 11111, 22222);

	var composed_path = new ComposedPath();
	composed_path.setPaths(paths);

	var map = new Map(coord_prize, imageBG, composed_path, grid, 10, 20, 50, 30, river_waves);
	map_river = map;
}

function createMapHalloween() {

	var grid = [
		[0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0],
		[0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0],
		[0,0,0,0,0,0,0,0,0,0,0,0,0,0,X,X,X,X,0,0],
		[0,0,0,0,X,X,X,X,X,X,X,X,X,X,0,0,0,X,X,0],
		[X,X,X,X,X,0,0,0,0,0,X,X,X,X,X,0,0,X,X,0],
		[0,X,X,X,X,X,X,0,0,X,X,0,0,0,X,0,0,X,X,0],
		[0,X,0,0,0,0,X,X,X,X,0,0,0,0,0,X,0,X,X,0],
		[0,X,0,0,0,0,0,X,X,X,X,0,0,0,0,X,0,X,0,0],
		[0,X,X,X,X,X,X,X,X,X,X,X,X,0,0,X,X,X,0,0],
		[0,0,0,0,0,0,0,X,X,X,0,0,X,X,X,X,X,0,0,0],
		[0,0,0,0,0,0,0,0,X,X,0,0,0,0,0,X,0,0,0,0],
		[0,0,0,0,0,0,0,0,0,0,0,0,0,0,X,X,0,0,0,0],
		[0,0,0,0,X,X,X,X,X,X,X,X,X,X,X,0,0,0,0,0],
		[0,0,0,X,0,0,0,0,X,X,X,X,X,0,0,0,0,0,0,0],
		[0,0,0,X,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0],
		[0,0,0,X,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0],
		[0,0,0,X,X,X,X,X,X,X,X,X,0,0,0,0,0,0,0,0],
		[0,0,0,0,0,0,0,0,0,0,0,X,X,0,0,0,0,0,0,0],
		[0,0,0,0,0,0,0,0,0,0,0,0,X,0,0,0,0,0,0,0],
		[X,X,X,X,X,X,X,X,X,X,X,X,X,X,X,X,X,X,X,X]
	];

	var paths = [
		new Path(
			new Point(-10, 171),
			new Point(51, 162),
			new Point(186, 215),
			new Point(136, 198)
		),new Path(
			new Point(186, 215),
			new Point(256, 245),
			new Point(477, 348),
			new Point(437, 328)
		),new Path(
			new Point(477, 348),
			new Point(567, 391),
			new Point(698, 333),
			new Point(662, 388)
		),new Path(
			new Point(698, 333),
			new Point(718, 293),
			new Point(722, 176),
			new Point(726, 226)
		),new Path(
			new Point(722, 176),
			new Point(722, 94),
			new Point(596, 115),
			new Point(636, 100)
		),new Path(
			new Point(596, 115),
			new Point(561, 125),
			new Point(451, 189),
			new Point(491, 159)
		),new Path(
			new Point(451, 189),
			new Point(396, 229),
			new Point(239, 336),
			new Point(314, 316)
		),new Path(
			new Point(239, 336),
			new Point(179, 356),
			new Point(63, 280),
			new Point(63, 355)
		),new Path(
			new Point(63, 280),
			new Point(63, 195),
			new Point(218, 150),
			new Point(148, 170)
		),new Path(
			new Point(218, 150),
			new Point(308, 140),
			new Point(517, 174),
			new Point(417, 144)
		),new Path(
			new Point(517, 174),
			new Point(567, 189),
			new Point(615, 275),
			new Point(615, 215)
		),new Path(
			new Point(615, 275),
			new Point(615, 325),
			new Point(602, 446),
			new Point(622, 391)
		),new Path(
			new Point(602, 446),
			new Point(587, 496),
			new Point(496, 518),
			new Point(541, 518)
		),new Path(
			new Point(496, 518),
			new Point(421, 528),
			new Point(212, 506),
			new Point(282, 511)
		),new Path(
			new Point(212, 506),
			new Point(177, 501),
			new Point(150, 535),
			new Point(160, 515)
		),new Path(
			new Point(150, 535),
			new Point(140, 560),
			new Point(128, 623),
			new Point(128, 588)
		),new Path(
			new Point(128, 623),
			new Point(138, 663),
			new Point(235, 674),
			new Point(190, 679)
		),new Path(
			new Point(235, 674),
			new Point(290, 674),
			new Point(412, 658),
			new Point(362, 648)
		),new Path(
			new Point(412, 658),
			new Point(467, 668),
			new Point(511, 735),
			new Point(511, 702)
		),new Path(
			new Point(511, 735),
			new Point(511, 755),
			new Point(510, 799),
			new Point(510, 774)
		)
	];

	/**********************************
	 *
	 *  Ja, hier sind die Koordinaten.
	 *  Du hast sie dir erschummelt,
	 *  bist du da drauf stolz?
	 *
	 *  Naja, nimm sie, wenn du willst.
	 *
	 * ********************************/

	var coord_prize = new Coords("GCTEST", 91291, 12838);

	var composed_path = new ComposedPath();
	composed_path.setPaths(paths);

	var map = new Map(coord_prize, imageBGFriedhof, composed_path, grid, 30, 30, 100, 10, halloween_waves);
	map_halloween = map;
}

class Map {

	constructor(coord_prize, image_background, composed_path, grid, rounds_to_win, cost_per_pass, start_gold, max_passed_enemies, waves) {
		this.coord_prize	= coord_prize;
		this.image_background	= image_background;
		this.composed_path	= composed_path;

		this.rounds_to_win = rounds_to_win;
		this.cost_per_pass = cost_per_pass;
		this.start_gold = start_gold;
		this.max_passed_enemies = max_passed_enemies;

		this.grid		= grid;
		this.grid_width		= this.grid[0].length;
		this.grid_height	= this.grid.length;

		this.waves		= waves;
	}

}

class Game {

	constructor(map) {

		this.negative_points	= 0;
		this.gold		= map.start_gold;
		this.map		= map;

		this.active_enemies	= [];
		this.active_bullets	= [];
		this.active_towers	= [];
		this.active_wave	= 1;

		this.tick_time		= 0;
		this.current_phase	= PHASE_MENU;

		this.tower_to_place = null;
	}

	setShowAOE(b) {
		for(let i = 0; i < this.active_towers.length; ++i) {
			this.active_towers[i].setDrawAOE(b);
		}
	}

	loadNextWave() {

		if(this.active_wave < this.map.waves.length) {

			this.active_enemies = [];
			let wave = this.map.waves[this.active_wave];

			for(let i = 0; i < wave.length; ++i) {

				let enemy_stub = wave[i];

				let type = enemy_stub[0];
				let spawn_time = enemy_stub[1];

				this.active_enemies.push(new Enemy(enemy_types[type][0], enemy_types[type][1], enemy_types[type][2], spawn_time));
			}

			this.tick_time = 0;
		}
	}

	transitionToPhase(phase) {

		this.current_phase = phase;

		if(this.current_phase == PHASE_PLAY) {

			textStatus.innerText = "Viel Glück!";

			draw_grid		= false;
			draw_grid_forbidden	= false;
			draw_path		= false;
			running			= true;

			// Reset Rest
			for(let i = 0; i < this.active_towers.length; ++i) {
				let t = this.active_towers[i];
				t.ticks_to_rest = 0;
			}

			// GRAY ALL BUTTONS
			grayButtons();
			this.loadNextWave();

		} else if(this.current_phase == PHASE_PLACE) {

			textStatus.innerText = "Aufbauphase";

			draw_grid		= true;
			draw_grid_forbidden	= false;
			draw_path		= false;
			running			= true;
			this.tower_to_place	= null;

			ungrayButtons();

			this.active_bullets = [];
			this.active_enemies = [];

		} else if(this.current_phase == PHASE_WIN) {

			var itemRaw = localStorage.getItem(itemNameSavedCoords);
			var coord_list = JSON.parse(itemRaw);

			if(coord_list == null) {
				coord_list = [];
			}

			coord_list.push(game.map.coord_prize);

			localStorage.setItem(itemNameSavedCoords, JSON.stringify(coord_list));

		} else {

			textStatus.innerText = "";

			draw_grid		= false;
			draw_grid_forbidden	= false;
			draw_path		= false;
			running			= true;

			this.tick_time = 0;

			this.active_wave	= 1;
			this.active_enemies	= [];
			this.active_towers	= [];

			// GRAY ALL BUTTONS

			grayButtons();

		}

	}

	removeEnemy(index) {
		this.active_towers = removeFromArray(this.active_towers, index);
	}

	removeBullet(index) {
		this.active_bullets = removeFromArray(this.active_bullets, index);
	}

	removeEnemy(index) {
		this.active_enemies = removeFromArray(this.active_enemies, index);
	}

	addGold(amount) {
		let g = this.gold + amount;
		this.gold = Math.max(0, g);
	}

	payForTower(tower, price) {
		if(this.gold >= price) {

			this.tower_to_place = tower;

			this.tower_to_place.setDrawAOE(true);
			this.setShowAOE(true);

			this.addGold(-price);

			setHUDText("Turm gekauft!", 50);
		} else {
			setHUDText("Nicht genug Gold!", 50);
		}
	}

	buyTower(type) {

		if(this.current_phase != PHASE_PLACE) {
			return;
		}

		if(this.tower_to_place != null) {
			setHUDText("Du hast noch einen Turm!", 50);
			return;
		}

		if(type == "Tradi") {

			this.payForTower(new TradiTower(0, 0), 50);

		} else if(type == "Multi") {

			this.payForTower(new MultiTower(0, 0), 100);

		} else if(type == "Mystery") {

			this.payForTower(new MysteryTower(0, 0), 200);

		} else if(type == "Earth") {

			this.payForTower(new EarthTower(0, 0), 500);
		}
	}

	evaluateTowers() {
		let k = this.active_towers.length;

		for(let i = 0; i < k; ++i) {

			let tower = this.active_towers[i];
			tower.tick(game);
		}
	}

	evaluateBullets() {

		let k = this.active_bullets.length;
		for(let i = 0; i < k; ++i) {

			let bullet = this.active_bullets[i];

			if(bullet != null) {
				bullet.tick(game);
			}
		}
	}

	evaluateEnemies() {

		for(let i = 0; i < this.active_enemies.length; ++i) {

			var enemy = this.active_enemies[i];

			if(this.tick_time >= enemy.spawn_time) {

				if(enemy.alive) {

					if(enemy.path_progression >= 1) {
						++this.negative_points;

						this.addGold(-this.map.cost_per_pass);

						enemy.alive = false;
						continue;
					}

					enemy.path_progression += enemy.speed/this.map.composed_path.total_length;

					var pos_point = this.map.composed_path.getPosAt(enemy.path_progression);
					enemy.setPosition(pos_point.x, pos_point.y);

				} else {
					this.addGold(5);
					this.active_enemies = removeFromArray(this.active_enemies, i);
				}
			}
		}

	}

	tick() {

		if(this.current_phase == PHASE_PLAY) {

			if(this.active_enemies.length > 0) {

				this.evaluateTowers();
				this.evaluateBullets();
				this.evaluateEnemies();
				++this.tick_time;

				this.addGold(1*(this.tick_time%32==0));

			} else {

				if(this.map.max_passed_enemies - this.negative_points > 0) {

					setHUDText("Welle geschafft!", 100);

					this.addGold(this.active_wave*10);

					if(this.active_wave == this.map.rounds_to_win) {

						console.log("TOTAL WIN");
						this.transitionToPhase(PHASE_WIN);

					} else {

						++this.active_wave;
						this.transitionToPhase(PHASE_PLACE);

					}

				} else {

					// GAME LOST.
					console.log("LOST!");
					this.transitionToPhase(PHASE_LOSE);

				}

			}

		} else if(this.current_phase == PHASE_PLACE) {

			// PLACING LOGIC

		}

	}

	drawBottomText() {

		var str = "Welle " + this.active_wave + "/" + this.map.rounds_to_win + "    Gold " + this.gold + "   Leben " + (this.map.max_passed_enemies-this.negative_points);

		ctx.font = '30px monospace';

		ctx.strokeStyle = 'black';
		ctx.lineWidth = 3;
		ctx.strokeText(str, canvas.width - 610, canvas.height - 10);

		ctx.fillStyle = 'white';
		ctx.fillText(str, canvas.width - 610, canvas.height - 10);
	}

	drawTowers() {
		for(let i = 0; i < this.active_towers.length; ++i) {
			this.active_towers[i].draw();
		}
	}

	drawBullets() {
		for(let i = 0; i < this.active_bullets.length; ++i) {
			this.active_bullets[i].draw();
		}
	}

	drawEnemies() {
		for(let i = 0; i < this.active_enemies.length; ++i) {
			var enemy = this.active_enemies[i];
			if(enemy.alive) {
				var pos = enemy.getPosition();
				ctx.drawImage(enemy.img, pos.x, pos.y, pixel_size, pixel_size);
			}
		}
	}

	drawGrid() {
		for(let y = 0; y < this.map.grid_height; ++y) {
			for(let x = 0; x < this.map.grid_width; ++x) {

				if(this.map.grid[y][x] == 0) {
					ctx.lineWidth = 1;
					ctx.strokeStyle = "green";
					ctx.strokeRect(x*pixel_size, y*pixel_size, pixel_size, pixel_size);
				} else if(draw_grid_forbidden) {
					ctx.fillStyle = "#FF000055";
					ctx.fillRect(x*pixel_size, y*pixel_size, pixel_size, pixel_size);
				}
			}
		}
	}

	draw() {

		ctx.clearRect(0, 0, canvas.width, canvas.height);

		if(this.current_phase == PHASE_PLAY) {

			ctx.drawImage(this.map.image_background, 0, 0);

			if(draw_grid) this.drawGrid();

			this.drawEnemies();
			this.drawTowers();
			this.drawBullets();
			this.drawBottomText();
			drawTopText();


		} else if(this.current_phase == PHASE_PLACE) {

			ctx.drawImage(this.map.image_background, 0, 0);

			if(this.tower_to_place != null) {
				this.tower_to_place.draw();

				let x = this.tower_to_place.grid_x;
				let y = this.tower_to_place.grid_y;

				if(this.map.grid[y][x] == X) {

					draw_grid_forbidden = true;

				} else {

					draw_grid_forbidden = false;

				}
			}

			if(draw_grid) this.drawGrid();

			this.drawTowers();
			this.drawBottomText();
			drawTopText();

		}



		if(draw_path) {
			for(let i = 0; i < paths.length; ++i) {
				drawPath(paths[i]);
			}
		}
	}

}

function loop() {


	time_end = Date.now();
	time_delta = time_end - time_start;

	let fps = 1000/100;

	if(time_delta > fps) {

		if(game != null) {

			game.tick();
			game.draw();

		}

		if(game == null || game.current_phase == PHASE_MENU) {
			// draw menu
			// do menu logic

			ctx.clearRect(0, 0, canvas.width, canvas.height);
			ctx.drawImage(imageMenu, 0, 0);

		} else if(game.current_phase == PHASE_LOSE) {

			ctx.clearRect(0, 0, canvas.width, canvas.height);
			ctx.drawImage(imageVerloren, 0, 0);

		} else if(game.current_phase == PHASE_WIN) {

			ctx.clearRect(0, 0, canvas.width, canvas.height);
			ctx.drawImage(imageGewonnen, 0, 0);
		}


		time_start = time_end - (time_delta % fps);
	}


	if(running) window.requestAnimationFrame(loop);
}

canvas.addEventListener('mousemove', function(e) {
	if(game != null && game.tower_to_place != null) {
		game.tower_to_place.grid_x = Math.floor(e.layerX/pixel_size);
		game.tower_to_place.grid_y = Math.floor(e.layerY/pixel_size);
	}
});

canvas.addEventListener('click', function(e) {
	if(game != null && game.tower_to_place != null) {

		if(game.map.grid[game.tower_to_place.grid_y][game.tower_to_place.grid_x] != X) {

			game.active_towers.push(game.tower_to_place);
			game.tower_to_place = null;

			game.setShowAOE(chkShowAOE.checked);

		} else {

			setHUDText("Hier nicht platzierbar!", 60);

		}
	}
});

chkShowAOE.addEventListener('click', function(e) {

	if(chkShowAOE.checked) {
		game.setShowAOE(true);

	} else {
		game.setShowAOE(false);
	}
});

buttonStartWave.onclick = function(e) {
	game.transitionToPhase(PHASE_PLAY);
}

buttonBuyTradi.onclick = function(e) {
	game.buyTower("Tradi");
}

buttonBuyMulti.onclick = function(e) {
	game.buyTower("Multi");
}

buttonBuyMystery.onclick = function(e) {
	game.buyTower("Mystery");
}

buttonBuyEarth.onclick = function(e) {
	game.buyTower("Earth");
}

document.addEventListener('DOMContentLoaded', function() {
	main();
});

/* MENU BUTTONS */

function hideMainMenu() {
	for(let i = 0; i < buttonsMainMenu.length; ++i) {
		buttonsMainMenu[i].style.display = 'none';
	}
}

function showMainMenu() {
	for(let i = 0; i < buttonsMainMenu.length; ++i) {
		buttonsMainMenu[i].style.display = '';
	}
}

function hideMapsMenu() {
	for(let i = 0; i < buttonsMapMenu.length; ++i) {
		buttonsMapMenu[i].style.display = "none";
	}
}

function showMapsMenu() {
	for(let i = 0; i < buttonsMapMenu.length; ++i) {
		buttonsMapMenu[i].style.display = "";
	}
}

function hideCoordsContainer() {
	containerCoords.style.display = 'none';
}

function showCoordsContainer() {
	containerCoords.style.display = '';
}

function hideCoordsButton() {
	buttonCoords.style.display = 'none';
}

function showCoordsButton() {
	buttonCoords.style.display = '';
}

buttonBack.onclick = function(e) {

	if(menu_state == MENU_SEL_MAPS) {
		menu_state = MENU_MAIN;

		hideMapsMenu();
		showMainMenu();

	} else if(menu_state == MENU_MANUAL) {
		menu_state = MENU_MAIN;
		/* LATER ?? */
	} else if(menu_state == MENU_GAME) {

		game.transitionToPhase(PHASE_MENU);
		game = null;

		menu_state = MENU_MAIN;

		hideMapsMenu();
		showMainMenu();

	} else if(menu_state == MENU_COORDS) {

		hideCoordsContainer();
		showMainMenu();

		menu_state = MENU_MAIN;

	}

	if(menu_state == MENU_MAIN) {

		if(localStorage.getItem(itemNameSavedCoords) == null) {
			hideCoordsButton();
		} else {
			showCoordsButton();
		}

		hideCoordsContainer();
	}
}

buttonManual.onclick = function(e) {
	/* LATER */
}

buttonCoords.onclick = function(e) {

	menu_state = MENU_COORDS;

	hideMainMenu();
	showCoordsContainer();

	loadCoordsFromLocalStorage();

}

buttonSelectMaps.onclick = function(e) {

	menu_state = MENU_SEL_MAPS;

	hideMainMenu();
	showMapsMenu();

}

buttonMapRiver.onclick = function(e) {
	game = new Game(map_river);

	hideMainMenu();
	hideMapsMenu();
	menu_state = MENU_GAME;

	game.transitionToPhase(PHASE_PLACE);
}

buttonMapHalloween.onclick = function(e) {

	game = new Game(map_halloween);

	hideMainMenu();
	hideMapsMenu();
	menu_state = MENU_GAME;

	game.transitionToPhase(PHASE_PLACE);
}
